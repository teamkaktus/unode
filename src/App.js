import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CreateNote from './pages/note/createNote.js';

class App extends Component {
  render() {
    return (
        <div>
            <nav id="mainNav" className="navbar navbar-default navbar-fixed-top navbar-custom padding_nav">
                <div className="container">
                    <div className="navbar-header page-scroll">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars"></i>
                        </button>
                        <a className="navbar-brand title_name_header logo_header" href="/"></a>
                    </div>

                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">
                            <li className="hidden">
                                <a href="#page-top"></a>
                            </li>
                            <li className="page-scroll">
                                <a href="#about">About</a>
                            </li>
                            <li className="page-scroll">
                                <a href="#contact">Contact</a>
                            </li>
                        </ul>
                    </div>

                </div>

            </nav>

            <section className="success padding_section" id="about">
                <div className="container">
                      {this.props.children}
                </div>
            </section>
        
            
        </div>
    );
  }
}

export default App;
