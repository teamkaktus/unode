import { combineReducers } from 'redux';
import noteReducer from  './reducers/noteReducer';
import subjectReducer from  './reducers/subjectReducer';

export default combineReducers ({
    note: noteReducer,
    subject: subjectReducer
});