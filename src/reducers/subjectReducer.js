export default (state = {}, action = {}) => {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case 'GET_SUBJECTS':
            console.log('GET_USERS', action.payload);
            return Object.assign({}, state, {
                subjects: action.payload,
            });
        case 'CREATE_SUBJECT':
            console.log('CREATE_NOTE', action.payload);
            return Object.assign({}, state, {
                subjects: [...state.subjects, action.payload]
            });
        case 'UPDATE_SUBJECT':
            let subjectsUpdate = [];
            state.subjects.map(note => {
                if (note.id == action.payload.id){
                    subjectsUpdate.push(action.payload)
                } else {
                    subjectsUpdate.push(note);
                }
            })
            return Object.assign({}, state, {
                subjects: subjectsUpdate
            });
        case 'DELETE_SUBJECT':
            let subjectsDelete = [];
            state.subjects.map(note => {
                if (note.id != action.id){
                    subjectsDelete.push(note);
                }
            })
            return Object.assign({}, state, {
                subjects: subjectsDelete
            });
        case 'ERROR_SUBJECTS':
            console.log('error',action.err);
            return newState;
        default:
            return newState;
    }
};