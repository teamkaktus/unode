import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import NotePage from './pages/NotePage';
import SubjectPage from './pages/SubjectPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';

export default (
<Route path="/" component={App}>
    <IndexRoute component={SubjectPage} />
    <Route path="subjects" component={SubjectPage}>
      
    </Route>
    <Route path="subjects/:id" component={NotePage}/>
    <Route path="login" component={LoginPage}/>
    <Route path="register" component={RegisterPage}/>
</Route>
);