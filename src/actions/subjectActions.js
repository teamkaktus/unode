import axios from '../axios-config';

export function createSubject(data) {
  const request = axios.post('subject_notes',data);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'CREATE_SUBJECT', payload: data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_SUBJECT', err });
    });
  };
}

export function getSubjects() {
  const request = axios.get('subject_notes');
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'GET_SUBJECTS', payload: data });
    }).catch((err) => {
      console.log('ERROR_SUBJECT', err)
      dispatch({ type: 'ERROR_SUBJECT', err });
    });
  };
}


export function updateSubject(data) {
  const request = axios.put('subject_notes',data);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'UPDATE_SUBJECT', payload: data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_SUBJECT', err });
    });
  };
}

export function deleteSubject(id) {
  const request = axios.delete('subject_notes/'+id);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'DELETE_SUBJECT', id });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_SUBJECT', err });
    });
  };
}