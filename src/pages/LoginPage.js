import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
// import Header from '../layout/Header';

class LoginPage extends React.Component {
   
    render() {
        return (
            <div className="row">
                <form className="form-signin">
                    <h2 className="form-signin-heading">Авторизація</h2>
                    <label for="inputEmail" className="sr-only">Email address</label>
                    <input type="email" id="inputEmail" className="form-control input_emai_style" placeholder="Email address" required="" autofocus=""/>
                    <label for="inputPassword" className="sr-only">Password</label>
                    <input type="password" id="inputPassword" className="form-control" placeholder="Password" required=""/>
                   
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Увійти</button>
                </form>
            </div>
        );
    }
}

export default connect((state) => {return state.pages}, {LoginPage})(LoginPage);