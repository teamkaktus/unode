import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { createSubject } from '../../actions/subjectActions';
// import Header from '../layout/Header';

class CreateSubject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            title: ''
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createSubject = this.createSubject.bind(this);
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

     handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    createSubject(){
        this.props.createSubject({title: this.state.title, user_id: 0});
        this.setState({
            showModal: false,
            title: '',
        })
    }

    render() {
        return (
            <div> 
                <button className="button_create_subject btn" onClick={this.open}>Створити тему</button>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Нова тема</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                        <div className="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" className="form-control" name="title" value={this.state.title} onChange={this.handleChange}/>
                        </div>
                    
                </Modal.Body>
                <Modal.Footer>
                    <Button className="button_modal_save btn" onClick={this.createSubject}>Зберегти</Button>
                    <Button className="button_modal_close btn" onClick={this.close}>Закрити</Button>
                </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default connect((state) => {return state.subject}, { createSubject })(CreateSubject);