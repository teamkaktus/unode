import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
// import Header from '../layout/Header';

class RegisterPage extends React.Component {
   
    render() {
        return (
            <div className="row">
                <form className="form-signin">
                    <h2 className="form-signin-heading">Реєстрація</h2>
                    <label for="inputName" className="sr-only">Name</label>
                    <input type="text" id="inputName" className="form-control input_name_style" placeholder="Name" required="" autofocus=""/>
                    <label for="inputEmail" className="sr-only">Email address</label>
                    <input type="email" id="inputEmail" className="form-control input_emai_style" placeholder="Email address" required="" autofocus=""/>
                    <label for="inputPassword" className="sr-only">Password</label>
                    <input type="password" id="inputPassword" className="form-control input_password_style" placeholder="Password" required=""/>
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Зареєструватися</button>
                </form>
            </div>
        );
    }
}

export default connect((state) => {return state.pages}, {RegisterPage})(RegisterPage);