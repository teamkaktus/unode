import React from 'react';
import Subject from './subject/subject.js';
import CreateSubject from './subject/createSubject.js';
import { connect } from 'react-redux';
import { getSubjects } from '../actions/subjectActions';
// import Header from '../layout/Header';

class SubjectPage extends React.Component {
    constructor(props) {
        super(props);
        this.props.getSubjects();
    }

    render() {
    
        return (
            <div className="row">
                <CreateSubject/>
                {this.props.subjects.map(subject => <Subject data={subject}/>)}
            </div>
        );
    }
}

export default connect((state) => {return state.subject}, {getSubjects})(SubjectPage);