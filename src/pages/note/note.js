import React from 'react';
import UpdateNote from './updateNote';
import { connect } from 'react-redux';
import { deleteNote } from '../../actions/noteActions';
// import Header from '../layout/Header';

class Note extends React.Component {
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }

    delete(){
        this.props.deleteNote(this.props.data.id);
    }

    render() {
        console.log('data', this.props.data);
        return (
            <div className="panel panel-info col-lg-4 container_note"> 
                <div className="panel-heading title_node">
                     <h3 className="panel-title">{this.props.data.name}</h3>
                </div>
                <div className="panel-body description_node"> {this.props.data.description} </div>
                <div className="container_button_note">
                     <UpdateNote data={this.props.data} subject_id={this.props.subject_id}/>
                     <button className="button_delete_subject btn" onClick={this.delete}>Видалити</button>
                </div>
            </div>
        );
    }
}

export default connect((state) => {return state.note}, { deleteNote })(Note);