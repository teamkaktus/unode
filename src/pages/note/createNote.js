import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { createNote } from '../../actions/noteActions';
// import Header from '../layout/Header';

class CreateNote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            name: '',
            description: ''
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createNote = this.createNote.bind(this);
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

     handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    createNote(){
        this.props.createNote({name: this.state.name, description: this.state.description, subject_id: this.props.subject_id});
        this.setState({
            showModal: false,
            name: '',
            description: ''})
    }

    render() {
        return (
            <div> 
                <button className="button_create_subject btn" onClick={this.open}>Створити замітку</button>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Створити замітку</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                        <div className="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label for="exampleInputPassword1">Disc</label>
                            <textarea className="form-control" name="description" value={this.state.description} onChange={this.handleChange}/>
                        </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="button_modal_save btn" onClick={this.createNote}>Зберегти</Button>
                    <Button className="button_modal_close btn" onClick={this.close}>Закрити</Button>
                </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default connect((state) => {return state.note}, { createNote })(CreateNote);